#include "header.h"

int main(int argc, char *argv[])
{
	FILE *ptrF;
	char buffer[N];
	int i, c = 0;
	if ((ptrF = fopen("entrada.txt", "r")) == NULL)
	{
		printf("El archivo no pudo abrirse\n");
		perror("Error: ");
	}
	else
	{
		while (fgets(buffer, N, ptrF))
		{
			c++;
		}
		char lineas[c + 1][N];
		c = 0;
		fseek(ptrF, 0, SEEK_SET);
		while (fgets(buffer, N, ptrF))
		{
			strcpy(lineas[c], buffer);
			printf("%s ", lineas[c]);
			c++;
		}
		fclose(ptrF);

		int vec[c][CANTCOLTXT];
		convertArray(c, &vec[0][0], lineas);

		for (i = 0; i < c; i++)
		{
			printf("Esperando %d segundos...\n", vec[i][2]);
			sleep	(vec[i][2]);
			printf("Mandando señal %d al proceso %d...\n", vec[i][1], vec[i][0]);
			kill(vec[i][0], vec[i][1]);
		}
		
	}
	return 0;
