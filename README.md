### Desarrolle dos programas

**Programa 1:** un proceso que envíe de forma planificada señales a los procesos detallados
en un archivo de texto. En cada fila de este archivo se indicará:
Por ejemplo:
| ProcesoId | Señal | SegundosDelay |
| --------- | ----- | ------------- |
| 12345 | 9 | 22 |
| 2222 | 15 | 4 |
| 3333 | 3 | 11 |

En este caso, el programa, una vez que arranca su ejecucion enviaría la señal 9 al proceso 12345 a los 22 segundos de haber arrancado, la señal 15 al proceso 2222 a los 4 segundos de arrancar y así sucesivamente. El programa termina cuando no queda ninguna planificación pendiente a enviar.

**Programa 2:** un proceso que implemente un handler que notifique cuando reciba las principales señales (SIGINT, SIGALRM, SIGUSR1, SIGUSR2, etc). Este proceso también será utilizado para validar el programa 1.
