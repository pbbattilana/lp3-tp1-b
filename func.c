#include "header.h"

void convertArray(int filas, int *vec, char matriz[filas][N])
{
    int i, j;
    char *token;

    for (i = 0; i < filas; i++)
    {
        j = 0;
        token = strtok(matriz[i], " ");
        while (token != 0)
        {
            *(vec++) = atoi(token);
            token = strtok(0, " ");
            j++;
        }
    }
}